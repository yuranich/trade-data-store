Trade Data Store
================

Sample application shows possible way of dealing with trade data streams and batches using *Apache HBase* and *Apache Phoenix*.

Start as spring boot app
------------------------
Could be started simply with `mvn spring-boot:run`

*Note*: requires hadoop environment set up.

API Usage
---------

REST API could be viewed via Swagger-UI by following link:
[Application Swagger UI](http://yuranich-server.ddns.net:8080/swagger-ui.html#/trade45controller)

1. One trade item or list of them could be put as JSON with 'Date' as long timestamp or string with format: "M/d/yyyy, h:mm:ss a".
   Ticker should by provided via URL parameter.
   
    Example request:

        PUT http://yuranich-server.ddns.net:8080/data/put/one?ticker=apple
    
        {
          "Trader Id": 3,
          "Date": "8/25/2014, 12:00:00 AM",
          "Share price (P)": 20.3,
          "Overall position": 5
        }
    
    
2. Batch of data could be loaded as .csv file. Example file could be generated via [CsvGenerator](https://bitbucket.org/yuranich/trade-data-store/raw/9eb233fe1f42041c1eecb1a59af27e6cf9dee598/src/main/java/home/yuranich/datastore/clientside/CsvGenerator.java)
    or you just can upload already prepared file [batch example](https://bitbucket.org/yuranich/trade-data-store/raw/9eb233fe1f42041c1eecb1a59af27e6cf9dee598/trade_batch.csv)
    
    Use it via Swagger at [URL](http://yuranich-server.ddns.net:8080/swagger-ui.html#!/trade45controller/saveBatchFastUnfilteredUsingPUT). You should also provide a ticker which will be stored for every row.
    
3. Uploaded results could be selected by query. Swagger UI doesn't support html rendering, so in swagger results could be viewed as JSON collection.

    To receive results as HTML table please send request directly through browser or other appropriate tool. Example request for browser:
    
    http://yuranich-server.ddns.net:8080/data/select?query=select%20*%20from%20trades%20limit%208

    Example query for dates range:

        select * from trades where event_date between to_time('2017-12-04 02:16:41') and to_time('2017-12-05 13:16:41') limit 5
        
4. There are several predefined queries exposed as API endpoints.
    Trade items for specified trader or ticker could be selected for last month. Example:
   
        http://yuranich-server.ddns.net:8080/data/lastMonth/html?traderId=4
        http://yuranich-server.ddns.net:8080/data/lastMonth/html?ticker=apple
   
    Or explicit dates range can be specified (with format: "M/d/yyyy, h:mm:ss a"). Example:
   
        http://yuranich-server.ddns.net:8080/data/dateRange/html?traderId=3&from=12%2F15%2F2013%2C%2003%3A34%3A41%20PM&to=12%2F15%2F2017%2C%2003%3A34%3A41%20PM
   
   
    Swagger UI json endpoints:
   
        http://yuranich-server.ddns.net:8080/swagger-ui.html#!/trade45controller/selectDateRangeUsingGET
        http://yuranich-server.ddns.net:8080/swagger-ui.html#!/trade45controller/selectLastMonthUsingGET
    
    
DB structure
-------------

HBAse table for storing trade data created via Phoenix. Table structure:


    CREATE TABLE IF NOT EXISTS TRADES
    (EVENT_DATE DATE NOT NULL,
    TRADER_ID INTEGER NOT NULL,
    PRICE FLOAT,
    POSITION INTEGER,
    CONSTRAINT PK PRIMARY KEY(EVENT_DATE ROW_TIMESTAMP, TRADER_ID))


So 'Date' from received data stored directly as timestamp in HBase. See [Row Timestamp feature](https://phoenix.apache.org/rowtimestamp.html) for details.


Testing
--------

Application has an extensive number of unit tests for all layers.

Load could be tested via Apache Jmeter. You could view [example scenario](https://bitbucket.org/yuranich/trade-data-store/raw/e7ca9428e9643e2a480f418e621e1d2ae908e22b/testload.jmx)


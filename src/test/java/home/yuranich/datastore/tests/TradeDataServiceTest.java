package home.yuranich.datastore.tests;

import home.yuranich.datastore.model.Trade;
import home.yuranich.datastore.repository.TradeDataRepository;
import home.yuranich.datastore.services.TradeDataService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static home.yuranich.datastore.repository.TradeDataRepository.inputFormatter;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeDataServiceTest {

    @Autowired
    private TradeDataService dataService;

    @MockBean
    private TradeDataRepository repository;

    @Mock
    private MultipartFile file;

    @Test
    public void testGetRecords() throws Exception {
        List<Trade> items = Collections.singletonList(
                new Trade(3, 1408950000000L, 20.3f, 5, "apple")
        );
        given(repository.getByQuery("select * from trades"))
                .willReturn(items);
        assertThat(dataService.getTrades("select * from trades"))
                .isEqualTo(items);
    }

    @Test
    public void testGetLastMonth() throws Exception {
        List<Trade> items = Collections.singletonList(
                new Trade(3, 1408950000000L, 20.3f, 5, "apple")
        );
        given(repository.selectListByFilters(eq(3),
                (String) Matchers.isNull(), any(LocalDateTime.class), (LocalDateTime) Matchers.isNull()))
                .willReturn(items);
        given(repository.selectListByFilters((Integer)Matchers.isNull(),
                eq("apple"), any(LocalDateTime.class), (LocalDateTime) Matchers.isNull()))
                .willReturn(items);

        assertThat(dataService.getAllTradesForTraderLastMonth(3))
                .isEqualTo(items);

        assertThat(dataService.getAllTradesForTickerLastMonth("apple"))
                .isEqualTo(items);
    }

    @Test
    public void testDatesRange() throws Exception {

        LocalDateTime from = LocalDateTime.parse("03/01/2017, 06:30:00 AM", inputFormatter).minusYears(2L);
        LocalDateTime to = LocalDateTime.parse("03/01/2017, 06:30:00 AM", inputFormatter).minusYears(1L);

        List<Trade> items = Collections.singletonList(
                new Trade(3, 1408950000000L, 20.3f, 5, "apple")
        );
        given(repository.selectListByFilters(3, null, from, to))
                .willReturn(items);
        given(repository.selectListByFilters(null, "apple", from, to))
                .willReturn(items);

        assertThat(dataService
                .getTradesForTraderInDatesRange(3, from.format(inputFormatter), to.format(inputFormatter)))
                .isEqualTo(items);

        assertThat(dataService
                .getTradesForTickerInDatesRange("apple", from.format(inputFormatter), to.format(inputFormatter)))
                .isEqualTo(items);
    }

    @Test
    public void testAsHTMLTable() throws Exception {
        String htmlTable = "<!DOCTYPE html><html><body><table><tr><th>Trader Id</th><th>Date</th>" +
                "<th>Share price (P)</th><th>Overall position</th><th>Ticker</th></tr><tr align='center'><td>3</td>" +
                "<td>8/25/2014, 7:00:00 AM</td><td>20.3</td><td>5</td><td>apple</td></tr></table></body></html>";
        List<Trade> items = Collections.singletonList(
                new Trade(3, 1408950000000L, 20.3f, 5, "apple")
        );

        assertThat(TradeDataService.asHtmlTable(items))
                .isEqualTo(htmlTable);
    }
}

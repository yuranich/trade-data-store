package home.yuranich.datastore.tests;

import home.yuranich.datastore.model.Trade;
import home.yuranich.datastore.repository.TradeDataRepository;
import home.yuranich.datastore.repository.TradeDataRepositoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@JdbcTest
@SpringBootTest
public class TradeDataRepositoryTest {

    @Autowired
    private JdbcTemplate template;

    @Autowired
    private TradeDataRepository repository;

    @Test
    public void testGetRow() throws Exception {
        Trade item = new Trade(3, 1408950000000L, 20.3f, 5, "apple");

        template.update("insert into trades values(3, '2014-08-25 07:00:00 UTC', 20.3, 5, 'apple')");
        assertThat(repository.getByQuery("select * from trades"))
                .isEqualTo(Collections.singletonList(item));
    }

    @Test
    public void testSelectByFilter() throws Exception {
        Trade item = new Trade(3, 1408950000000L, 20.3f, 5, "apple");
        template.update("insert into trades values(3, '2014-08-25 07:00:00 UTC', 20.3, 5, 'apple')");
        assertThat(repository.selectListByFilters(3, null, null, null))
                .isEqualTo(Collections.singletonList(item));

        assertThat(repository.selectListByFilters(10, null, null, null))
                .isEmpty();

        assertThat(repository.selectListByFilters(null, "apple", LocalDateTime.of(2012, 5, 7, 3, 30), null))
                .isEqualTo(Collections.singletonList(item));

        assertThat(repository.selectListByFilters(null, "unknown", LocalDateTime.of(2012, 5, 7, 3, 30), null))
                .isEmpty();

        assertThat(repository.selectListByFilters(3, "apple", null, null))
                .isEqualTo(Collections.singletonList(item));

        assertThat(repository.selectListByFilters(3, "unknown", null, null))
                .isEmpty();

        assertThat(repository.selectListByFilters(3, null, LocalDateTime.of(2012, 5, 7, 3, 30), LocalDateTime.now()))
                .isEqualTo(Collections.singletonList(item));

        assertThat(repository
                .selectListByFilters(3, null, LocalDateTime.now().minusMonths(1L), LocalDateTime.now()))
                .isEmpty();

    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSelectWithNoStartDate() {
        repository.selectListByFilters(null, "apple", null, null);
    }

    @TestConfiguration
    public static class ConfigureDataSource {

        @Bean
        public TradeDataRepository repository() {
            return new TradeDataRepositoryImpl();
        }
    }
}

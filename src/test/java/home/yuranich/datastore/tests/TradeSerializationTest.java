package home.yuranich.datastore.tests;

import home.yuranich.datastore.model.Trade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@JsonTest
public class TradeSerializationTest {

    private static final String TEST_DATE_FORMATTED = "08/25/2014, 07:00:00 AM";
    private static final long TEST_TIMESTAMP = 1408950000000L;

    @Autowired
    private JacksonTester<Trade> tester;

    @Test
    public void testDateSerialize() throws Exception {
        Trade item = new Trade(1, TEST_TIMESTAMP, 25.17f, 42, "apple");

        assertThat(tester.write(item)).hasJsonPathStringValue("@.Date");
        assertThat(tester.write(item)).extractingJsonPathStringValue("@.Date").isEqualTo(TEST_DATE_FORMATTED);
    }

    @Test
    public void testDeserialize() throws Exception {
        String content = getResourceAsString("test.json");

        assertThat(tester.parseObject(content).getDate()).isEqualTo(TEST_TIMESTAMP);
    }

    private String getResourceAsString(String name) throws IOException {
        try (InputStream resource = this.getClass().getClassLoader().getResourceAsStream(name);
             BufferedReader buffer = new BufferedReader(new InputStreamReader(resource))) {
            return buffer.lines().collect(Collectors.joining("\n"));
        }
    }
}

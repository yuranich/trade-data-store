package home.yuranich.datastore.tests;

import home.yuranich.datastore.controllers.TradeController;
import home.yuranich.datastore.model.Trade;
import home.yuranich.datastore.services.TradeDataService;
import home.yuranich.datastore.utils.ResourcesUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TradeController.class)
public class TradeControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TradeDataService dataService;

    @Test
    public void testHealth() throws Exception {
        mvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(content().string("App is ready to work!"));
    }

    @Test
    public void testPutOne() throws Exception {
        String content = ResourcesUtils.getResourceAsString("test.json", this.getClass().getClassLoader());
        mvc.perform(put("/data/put/one").param("ticker", "apple")
                .contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(status().isCreated())
                .andExpect(content().string("trade 3 will be saved"));
    }

    @Test
    public void testPutList() throws Exception {
        String content =
                "[" + ResourcesUtils.getResourceAsString("test.json", this.getClass().getClassLoader()) + "]";
        mvc.perform(put("/data/put/list").param("ticker", "apple")
                .contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(status().isCreated())
                .andExpect(content().string("1 trades will be saved"));
    }

    @Test
    public void testPutBulk() throws Exception {
        String content = ResourcesUtils.getResourceAsString("test_batch.csv", this.getClass().getClassLoader());
        MockMultipartFile file = new MockMultipartFile(
                "file", "test_batch.csv", MediaType.MULTIPART_FORM_DATA_VALUE, content.getBytes()
        );
        mvc.perform(fileUpload("/data/batch/csv")
                .file(file).with(request -> {
                    request.setMethod("PUT");
                    return request;
                }).param("ticker", "apple"))
                .andExpect(status().isCreated())
                .andExpect(content().string("batch file test_batch.csv will be saved"));
    }

    @Test
    public void testSelect() throws Exception {
        Trade item = new Trade(3, 1408950000000L, 20.3f, 5, "apple");
        String jsItem = ResourcesUtils.getResourceAsString("test.json", this.getClass().getClassLoader());
        String collection = "[" + jsItem + "]";
        given(dataService.getTrades("select *"))
                .willReturn(Collections.singletonList(item));
        mvc.perform(get("/data/select/json").param("query", "select *")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().json(collection));
    }

    @Test
    public void testGetLastMonth() throws Exception {
        Trade item = new Trade(3, 1408950000000L, 20.3f, 5, "apple");
        String jsItem = ResourcesUtils.getResourceAsString("test.json", this.getClass().getClassLoader());
        String collection = "[" + jsItem + "]";
        given(dataService.getAllTradesForTraderLastMonth(3))
                .willReturn(Collections.singletonList(item));
        given(dataService.getAllTradesForTickerLastMonth("apple"))
                .willReturn(Collections.singletonList(item));

        mvc.perform(get("/data/lastMonth/json").param("traderId", "3")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().json(collection));

        mvc.perform(get("/data/lastMonth/json").param("ticker", "apple")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().json(collection));
    }

    @Test
    public void testGetDatesRange() throws Exception {
        Trade item = new Trade(3, 1408950000000L, 20.3f, 5, "apple");
        String jsItem = ResourcesUtils.getResourceAsString("test.json", this.getClass().getClassLoader());
        String collection = "[" + jsItem + "]";
        given(dataService.getTradesForTraderInDatesRange(
                3, "08/25/2014, 07:00:00 AM", "08/25/2015, 07:00:00 AM"))
                .willReturn(Collections.singletonList(item));
        given(dataService.getTradesForTickerInDatesRange(
                "apple", "08/25/2014, 07:00:00 AM", "08/25/2015, 07:00:00 AM"))
                .willReturn(Collections.singletonList(item));

        mvc.perform(get("/data/dateRange/json")
                .param("traderId", "3")
                .param("from", "08/25/2014, 07:00:00 AM")
                .param("to", "08/25/2015, 07:00:00 AM")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().json(collection));

        mvc.perform(get("/data/dateRange/json")
                .param("ticker", "apple")
                .param("from", "08/25/2014, 07:00:00 AM")
                .param("to", "08/25/2015, 07:00:00 AM")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().json(collection));
    }
}

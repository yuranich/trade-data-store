package home.yuranich.datastore.repository;

import home.yuranich.datastore.model.Trade;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by yury on 02.12.17.
 */
public interface TradeDataRepository {

    /**
     * Required input dates format which this service able to handle.
     */
    DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("M/d/yyyy, h:mm:ss a");

    /**
     * Timestamp compatible date format used to output DateTime to database.
     */
    DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * Saves one trade to DB.
     *
     * @param trade - item to save as a single row.
     */
    void saveOne(Trade trade);

    /**
     * Saves collection of trades to DB.
     *
     * @param trades
     */
    void saveSmallBatch(List<Trade> trades);

    /**
     * Selects list of items from DB by specified Phoenix query.
     *
     * @param query - to be executed on HBase via Phoenix engine.
     * @return - collection of selected items.
     */
    List<Trade> getByQuery(String query);

    /**
     * Select list of items from DB filtered by input values.
     *
     * @param traderId - Id of the trader for whom items should be selected.
     * @param ticker - ticker to filter by
     * @param from - start date for range
     * @param to - end date for range
     * @return List of trade items which meet the filters.
     */
    List<Trade> selectListByFilters(Integer traderId, String ticker, LocalDateTime from, LocalDateTime to);

    /**
     * Saves a batch of trade items to DB.
     * File provided as open stream which won't be closed here.
     *
     * @param ticker - ticker for whick data will be saved.
     * @param batchFile - stream of data from received file
     */
    void saveBatchForTicker(String ticker, InputStream batchFile);
}

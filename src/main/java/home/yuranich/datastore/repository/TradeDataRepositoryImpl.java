package home.yuranich.datastore.repository;

import home.yuranich.datastore.model.Trade;
import home.yuranich.datastore.utils.ResourcesUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameterValue;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by yury on 02.12.17.
 */
@Repository
public class TradeDataRepositoryImpl implements TradeDataRepository {
    private static final int MAX_LIMIT = 100;
    private static final String TABLE_NAME = "TRADES";

    private static final Logger logger = LoggerFactory.getLogger(TradeDataRepositoryImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    @Transactional
    public void saveOne(Trade trade) {
        try {
            LocalDateTime time = LocalDateTime.ofInstant(Instant.ofEpochMilli(trade.getDate()), ZoneOffset.UTC);
            jdbcTemplate.update(Queries.UPDATE_OR_INSERT_ROW,
                    new SqlParameterValue(Types.INTEGER, trade.getTraderId()),
                    new SqlParameterValue(Types.VARCHAR, time.format(outputFormatter)),
                    new SqlParameterValue(Types.FLOAT, trade.getPrice()),
                    new SqlParameterValue(Types.INTEGER, trade.getPosition()),
                    new SqlParameterValue(Types.VARCHAR, trade.getTicker())
            );
        } catch (DataAccessException e) {
            logger.error("Failed to upsert a row!!!", e);
            throw e;
        }
    }

    @Override
    @Transactional
    public void saveSmallBatch(List<Trade> trades) {
        try {
            jdbcTemplate.batchUpdate(Queries.UPDATE_OR_INSERT_ROW, trades, trades.size(), ((ps, argument) -> {
                ps.setInt(1, argument.getTraderId());
                LocalDateTime timestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(argument.getDate()), ZoneOffset.UTC);
                ps.setString(2, timestamp.format(outputFormatter));
                ps.setFloat(3, argument.getPrice());
                ps.setInt(4, argument.getPosition());
                ps.setString(5, argument.getTicker());
            }));
        } catch (DataAccessException e) {
            logger.error("Failed to upsert a row!!!", e);
            throw e;
        }
    }

    @Override
    public void saveBatchForTicker(String ticker, InputStream batchFile) {
        try {
            Configuration configuration = new Configuration();
            Path dest = new Path("/bulk/testdir");
            try (FileSystem fs = FileSystem.get(new URI("hdfs://localhost:9000"), configuration);
                 FSDataOutputStream out = fs.create(dest, true);
                 CSVParser parser = CSVFormat.DEFAULT
                         .withHeader("Trader Id", "Date", "Share price (P)", "Overall position")
                         .withSkipHeaderRecord(true).parse(new InputStreamReader(batchFile))) {

                final RecordTransformer trasformer = new RecordTransformer(ticker);
                parser.forEach(record -> {
                    String preparedOutput = trasformer.apply(record);
                    try {
                        out.writeChars(preparedOutput);
                    } catch (IOException e) {
                        logger.error("Failed to write next row from csv to hdfs file! ", e);
                    }
                });
                out.hflush();
            }
            logger.debug("batch file saved to hdfs.");

            String command = ResourcesUtils.getResourceAsString("bulkload.sh", this.getClass().getClassLoader());
            Process process = Runtime.getRuntime().exec(command);
            logger.info("Finished bulk job successfully: {}", process.waitFor(10, TimeUnit.MINUTES));
        } catch (URISyntaxException | IOException | InterruptedException e) {
            logger.error("Bulk load failed for ticker: {}. Error:", ticker, e);
        }
    }

    @Override
    public List<Trade> getByQuery(String query) {
        logger.info("Received query: {}", query);
        List<Trade> rows = new ArrayList<>();
        jdbcTemplate.setMaxRows(MAX_LIMIT);
        jdbcTemplate.query(query, row -> {
            int traderId = row.getInt(1);
            Timestamp date = row.getTimestamp(2);
            float price = row.getFloat(3);
            int position = row.getInt(4);
            String ticker = row.getString(5);
            rows.add(new Trade(traderId, date.toInstant().toEpochMilli(), price, position, ticker));
        });
        logger.info("Select {} rows.", rows.size());
        return rows;
    }

    @Override
    public List<Trade> selectListByFilters(Integer traderId, String ticker, LocalDateTime from, LocalDateTime to) {
        jdbcTemplate.setMaxRows(MAX_LIMIT);
        final SqlParameterValue[] fullArgs = new SqlParameterValue[]{
                new SqlParameterValue(Types.INTEGER, traderId),
                new SqlParameterValue(Types.VARCHAR, ticker),
                new SqlParameterValue(Types.TIMESTAMP, from == null ? null : Timestamp.valueOf(from)),
                new SqlParameterValue(Types.TIMESTAMP, to == null ? null : Timestamp.valueOf(to))
        };
        String query = null;
        SqlParameterValue[] args = null;
        if (traderId != null) {
            if (ticker != null) {
                if (from != null && to != null) {
                    query = Queries.SELECT_BY_TRADER_ID_AND_TICKER_AND_DATES;
                    args = generateArgsByIndices(fullArgs, 0, 1, 2, 3);
                } else if (from != null) {
                    query = Queries.SELECT_BY_TRADER_ID_AND_TICKER_AND_FROM;
                    args = generateArgsByIndices(fullArgs, 0, 1, 2);
                } else {
                    query = Queries.SELECT_BY_TRADER_ID_AND_TICKER;
                    args = generateArgsByIndices(fullArgs, 0, 1);
                }
            } else {
                if (from != null && to != null) {
                    query = Queries.SELECT_BY_TRADER_AND_DATES;
                    args = generateArgsByIndices(fullArgs, 0, 2, 3);
                } else if (from != null) {
                    query = Queries.SELECT_BY_TRADER_AND_FROM;
                    args = generateArgsByIndices(fullArgs, 0, 2);
                } else {
                    query = Queries.SELECT_BY_TRADER_ID;
                    args = generateArgsByIndices(fullArgs, 0);
                }
            }
        } else {
            if (ticker != null) {
                if (from != null && to != null) {
                    query = Queries.SELECT_BY_TICKER_AND_DATES;
                    args = generateArgsByIndices(fullArgs, 1, 2, 3);
                } else if (from != null) {
                    query = Queries.SELECT_BY_TICKER_AND_FROM;
                    args = generateArgsByIndices(fullArgs, 1, 2);
                } else {
                    throw new UnsupportedOperationException("Unsupported filter: no from date specified.");
                }
            }
        }
        List<Trade> rows = new ArrayList<>();
        jdbcTemplate.query(query, args, row -> {
            int id = row.getInt(1);
            Timestamp date = row.getTimestamp(2);
            float price = row.getFloat(3);
            int position = row.getInt(4);
            String tick = row.getString(5);
            rows.add(new Trade(id, date.getTime(), price, position, tick));
        });
        logger.debug("Selected {} rows from DB.", rows.size());
        return rows;
    }

    private SqlParameterValue[] generateArgsByIndices(SqlParameterValue[] extractFrom, int... indices) {
        return Arrays.stream(indices)
                .mapToObj(i -> extractFrom[i])
                .collect(Collectors.toList()).toArray(new SqlParameterValue[indices.length]);
    }

    private static class RecordTransformer implements Function<CSVRecord, String> {
        private final String ticker;

        public RecordTransformer(String ticker) {
            this.ticker = ticker;
        }

        @Override
        public String apply(CSVRecord record) {
            StringBuilder transformed = new StringBuilder(40);
            transformed.append(record.get(0)).append(',');
            String date = LocalDateTime.parse(record.get(1), inputFormatter).format(outputFormatter);
            transformed
                    .append(date).append(',')
                    .append(record.get(2)).append(',')
                    .append(record.get(3)).append(',')
                    .append(ticker).append('\n');
            logger.debug("Transformed row: {}", transformed.toString());
            return transformed.toString();
        }
    }

    private static final class Queries {
        private static final String UPDATE_OR_INSERT_ROW = "UPSERT INTO TRADES VALUES (?, ?, ?, ?, ?)";

        private static final String SELECT_BY_TRADER_ID = "SELECT * FROM TRADES WHERE TRADER_ID = ?";
        private static final String SELECT_BY_TRADER_AND_DATES = "SELECT * FROM TRADES \n" +
                "WHERE TRADER_ID = ? \n" +
                "  AND EVENT_DATE BETWEEN ? AND ?";
        private static final String SELECT_BY_TRADER_AND_FROM = "SELECT * FROM TRADES \n" +
                "WHERE TRADER_ID = ? \n" +
                "  AND EVENT_DATE > ?";
        private static final String SELECT_BY_TRADER_ID_AND_TICKER = "SELECT * FROM TRADES \n" +
                "WHERE TRADER_ID = ?\n" +
                "  AND TICKER = ?";
        private static final String SELECT_BY_TRADER_ID_AND_TICKER_AND_DATES = "SELECT * FROM TRADES \n" +
                "WHERE TRADER_ID = ?\n" +
                "  AND TICKER = ?\n" +
                "  AND EVENT_DATE BETWEEN ? AND ?";
        private static final String SELECT_BY_TRADER_ID_AND_TICKER_AND_FROM = "SELECT * FROM TRADES \n" +
                "WHERE TRADER_ID = ?\n" +
                "  AND TICKER = ?\n" +
                "  AND EVENT_DATE > ?";
        private static final String SELECT_BY_TICKER_AND_DATES = "SELECT * FROM TRADES \n" +
                "WHERE TICKER = ?\n" +
                "  AND EVENT_DATE BETWEEN ? AND ?";
        private static final String SELECT_BY_TICKER_AND_FROM = "SELECT * FROM TRADES \n" +
                "WHERE TICKER = ?\n" +
                "  AND EVENT_DATE > ?";


    }
}

package home.yuranich.datastore.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * Created by yury on 03.12.17.
 */
public class TradeDeserializer extends StdDeserializer<Trade> {

    private static final Logger logger = LoggerFactory.getLogger(TradeDeserializer.class);
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy, h:m:s a");

    public TradeDeserializer() {
        this(null);
    }

    public TradeDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Trade deserialize(JsonParser jp, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        int id = node.get("Trader Id").intValue();
        float price = node.get("Share price (P)").floatValue();
        int position = node.get("Overall position").intValue();
        String ticker = Optional.ofNullable(node.get("Ticker"))
                .map(JsonNode::textValue)
                .orElse(null);
        JsonNode dateNode = node.get("Date");
        JsonNodeType nodeType = dateNode.getNodeType();
        long date = -1;
        switch (nodeType) {
            case NUMBER:
                date = dateNode.numberValue().longValue();
                logger.debug("date deserialized from long timestamp: {}", date);
                break;
            case STRING:
                String strDate = dateNode.textValue();
                date = LocalDateTime.parse(strDate, formatter).toInstant(ZoneOffset.UTC).toEpochMilli();
                logger.debug("date deserialized from formatted string: {}", date);
                break;
            default: throw new IllegalArgumentException("Could not parse 'Date' from unsupported type: " + nodeType);
        }
        if (date < 0) {
            throw new IllegalArgumentException("Received invalid date in json node: " + dateNode.toString());
        }
        return new Trade(id, date, price, position, ticker);
    }
}

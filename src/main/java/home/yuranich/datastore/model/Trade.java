package home.yuranich.datastore.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yury on 20.11.17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonDeserialize(using = TradeDeserializer.class)
public class Trade {
    @JsonProperty("Trader Id")
    private int traderId;

    @JsonProperty("Date")
    @JsonSerialize(using = TimestampToDateSerializer.class)
    private long date;

    @JsonProperty("Share price (P)")
    private float price;

    @JsonProperty("Overall position")
    private int position;

    @JsonProperty("Ticker")
    private String ticker;
}

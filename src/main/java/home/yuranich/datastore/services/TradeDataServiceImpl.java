package home.yuranich.datastore.services;

import com.google.protobuf.ServiceException;
import home.yuranich.datastore.model.Trade;
import home.yuranich.datastore.repository.TradeDataRepository;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static home.yuranich.datastore.repository.TradeDataRepository.inputFormatter;

/**
 * Created by yury on 20.11.17.
 */
@Service
public class TradeDataServiceImpl implements TradeDataService {

    private static final Logger logger = LoggerFactory.getLogger(TradeDataServiceImpl.class);

    private ExecutorService executor;

    @Autowired
    private TradeDataRepository repository;

    @PostConstruct
    public void init() throws IOException, ServiceException {
        executor = Executors.newCachedThreadPool();
    }

    @PreDestroy
    public void destroy() throws IOException {
        executor.shutdown();
        logger.debug("pre destroy finished ===============");
    }

    @Override
    public void saveTradeForTicker(String ticker, Trade record) {
        Optional.ofNullable(ticker).ifPresent(record::setTicker);
        executor.execute(() -> repository.saveOne(record));
    }

    @Override
    public void saveTradeListForTicker(String ticker, List<Trade> trades) {
        if (StringUtils.isNotEmpty(ticker)) {
            trades.forEach(t -> t.setTicker(ticker));
        }
        executor.execute(() -> repository.saveSmallBatch(trades));
    }

    @Override
    public void saveTradesBatchForTicker(String ticker, MultipartFile batch) {
        executor.execute(() -> {
            try (InputStream stream = batch.getInputStream()) {
                repository.saveBatchForTicker(ticker, stream);
            } catch (IOException e) {
                logger.error("Failed to read received batch file[{}].", batch.getName(), e);
            }
        });
    }

    @Override
    public List<Trade> getTrades(String query) {
        return repository.getByQuery(query);
    }

    @Override
    public List<Trade> getAllTradesForTraderLastMonth(int traderId) {
        LocalDateTime from = LocalDateTime.now().minusMonths(1L);
        return repository.selectListByFilters(traderId, null, from, null);
    }

    @Override
    public List<Trade> getAllTradesForTickerLastMonth(String ticker) {
        LocalDateTime from = LocalDateTime.now().minusMonths(1L);
        return repository.selectListByFilters(null, ticker, from, null);
    }

    @Override
    public List<Trade> getTradesForTraderInDatesRange(int traderId, String from, String to) {
        LocalDateTime fromDate = LocalDateTime.parse(from, inputFormatter);
        LocalDateTime toDate = LocalDateTime.parse(to, inputFormatter);
        return repository.selectListByFilters(traderId, null, fromDate, toDate);
    }

    @Override
    public List<Trade> getTradesForTickerInDatesRange(String ticker, String from, String to) {
        LocalDateTime fromDate = LocalDateTime.parse(from, inputFormatter);
        LocalDateTime toDate = LocalDateTime.parse(to, inputFormatter);
        return repository.selectListByFilters(null, ticker, fromDate, toDate);
    }

}

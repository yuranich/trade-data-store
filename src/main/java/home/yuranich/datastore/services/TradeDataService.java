package home.yuranich.datastore.services;

import home.yuranich.datastore.model.Trade;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static home.yuranich.datastore.repository.TradeDataRepository.inputFormatter;

public interface TradeDataService {

    /**
     * Asyncronously stores received trade item to DB.
     *
     * @param ticker - the channel of data consuming.
     * @param trade - model object with filled fields.
     *
     */
    void saveTradeForTicker(String ticker, Trade trade);

    /**
     * Asyncronously stores list of received trade items to DB.
     *
     * @param ticker - the channel of data consuming.
     * @param trades - list of model objects with filled fields.
     */
    void saveTradeListForTicker(String ticker, List<Trade> trades);

    /**
     * Asyncronously stores received batch file to DB.
     *
     * @param ticker - the channel of data consuming.
     * @param batch - file with multiple items to be saved in data store.
     *
     */
    void saveTradesBatchForTicker(String ticker, MultipartFile batch);

    /**
     * Selects items from DB by query and returns them as html table.
     *
     * @param trades - list of selected items from database.
     * @return - HTML table with results.
     *
     */
    static String asHtmlTable(List<Trade> trades) {
        StringBuilder table = new StringBuilder("<!DOCTYPE html><html><body><table><tr><th>Trader Id</th><th>Date</th><th>Share price (P)</th><th>Overall position</th><th>Ticker</th></tr>");
        trades.forEach(trade -> {
            LocalDateTime time = LocalDateTime.ofInstant(Instant.ofEpochMilli(trade.getDate()), ZoneOffset.UTC);
            table.append("<tr align='center'><td>")
                    .append(trade.getTraderId())
                    .append("</td><td>")
                    .append(time.format(inputFormatter))
                    .append("</td><td>")
                    .append(trade.getPrice())
                    .append("</td><td>")
                    .append(trade.getPosition())
                    .append("</td><td>")
                    .append(trade.getTicker())
                    .append("</td></tr>");
        });
        table.append("</table></body></html>");
        return table.toString();
    }

    /**
     * Selects items from DB by query and returns them as a collection.
     *
     * @param query - to be used to select required items.
     * @return - the collection of Trade items.
     *
     */
    List<Trade> getTrades(String query);

    /**
     * Returns all trade items for the specified trader for last month.
     *
     */
    List<Trade> getAllTradesForTraderLastMonth(int traderId);

    /**
     * Returns all trade items for the specified ticker for last month.
     *
     */
    List<Trade> getAllTradesForTickerLastMonth(String ticker);

    /**
     * Returns all trade items for the specified trader and dates range.
     * Dates should be formatted as 'MM/dd/yyyy, hh:mm:ss a' and passed as String
     * Example '08/25/2014, 12:00:00 AM'
     *
     */
    List<Trade> getTradesForTraderInDatesRange(int traderId, String from, String to);

    /**
     * Returns all trade items for the specified ticker and dates range.
     * Dates should be formatted as 'MM/dd/yyyy, hh:mm:ss a' and passed as String
     * Example '08/25/2014, 12:00:00 AM'
     *
     */
    List<Trade> getTradesForTickerInDatesRange(String ticker, String from, String to);
}

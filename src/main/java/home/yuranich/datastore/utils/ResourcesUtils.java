package home.yuranich.datastore.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public final class ResourcesUtils {
    public static String getResourceAsString(String name, ClassLoader loader) throws IOException {
        try (InputStream resource = loader.getResourceAsStream(name);
             BufferedReader buffer = new BufferedReader(new InputStreamReader(resource))) {
            return buffer.lines().collect(Collectors.joining("\n"));
        }
    }
}

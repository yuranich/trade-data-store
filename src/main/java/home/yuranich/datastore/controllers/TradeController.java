package home.yuranich.datastore.controllers;

import home.yuranich.datastore.model.Trade;
import home.yuranich.datastore.services.TradeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class TradeController {

    @Autowired
    private TradeDataService tradeDataService;

    @GetMapping("/")
    public String index() {
        return "App is ready to work!";
    }

    @PutMapping(value = "/data/put/one", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public String saveItem(@RequestParam(value = "ticker", required = false) String ticker, @RequestBody Trade row) {
        tradeDataService.saveTradeForTicker(ticker, row);
        return String.format("trade %s will be saved", row.getTraderId());
    }

    @PutMapping(value = "/data/put/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public String saveItem(@RequestParam(value = "ticker", required = false) String ticker, @RequestBody List<Trade> rows) {
        tradeDataService.saveTradeListForTicker(ticker, rows);
        return String.format("%s trades will be saved", rows.size());
    }

    @PutMapping(value = "/data/batch/csv", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public String saveBatchFastUnfiltered(@RequestParam("ticker") String ticker, @RequestParam("file") MultipartFile csvFile) {
        tradeDataService.saveTradesBatchForTicker(ticker, csvFile);
        return String.format("batch file %s will be saved", csvFile.getOriginalFilename());
    }

    @GetMapping(value = "/data/lastMonth/json", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Trade> selectLastMonth(@RequestParam(value = "traderId", required = false) Integer traderId,
                                                @RequestParam(value = "ticker", required = false) String ticker) {
        if (traderId != null) {
            return tradeDataService.getAllTradesForTraderLastMonth(traderId);
        } else if (ticker != null) {
            return tradeDataService.getAllTradesForTickerLastMonth(ticker);
        } else {
            throw new IllegalArgumentException("At least one of the parameters (traderId, ticker) should be specified.");
        }
    }

    @GetMapping(value = "/data/lastMonth/html", produces = MediaType.TEXT_HTML_VALUE)
    public String selectLastMonthHtml(@RequestParam(value = "traderId", required = false) Integer traderId,
                                       @RequestParam(value = "ticker", required = false) String ticker) {
        List<Trade> result = null;
        if (traderId != null) {
            result = tradeDataService.getAllTradesForTraderLastMonth(traderId);
        } else if (ticker != null) {
            result = tradeDataService.getAllTradesForTickerLastMonth(ticker);
        } else {
            throw new IllegalArgumentException("At least one of the parameters (traderId, ticker) should be specified.");
        }
        return TradeDataService.asHtmlTable(result);
    }

    @GetMapping(value = "/data/dateRange/json", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Trade> selectDateRange(@RequestParam(value = "traderId", required = false) Integer traderId,
                                       @RequestParam(value = "ticker", required = false) String ticker,
                                       @RequestParam(value = "from") String from,
                                       @RequestParam(value = "to") String to) {
        if (traderId != null) {
            return tradeDataService.getTradesForTraderInDatesRange(traderId, from, to);
        } else if (ticker != null) {
            return tradeDataService.getTradesForTickerInDatesRange(ticker, from, to);
        } else {
            throw new IllegalArgumentException("At least one of the parameters (traderId, ticker) should be specified.");
        }
    }

    @GetMapping(value = "/data/dateRange/html", produces = MediaType.TEXT_HTML_VALUE)
    public String selectDateRangeHtml( @RequestParam(value = "traderId", required = false) Integer traderId,
                                       @RequestParam(value = "ticker", required = false) String ticker,
                                       @RequestParam(value = "from") String from,
                                       @RequestParam(value = "to") String to) {
        List<Trade> result = null;
        if (traderId != null) {
            result = tradeDataService.getTradesForTraderInDatesRange(traderId, from, to);
        } else if (ticker != null) {
            result = tradeDataService.getTradesForTickerInDatesRange(ticker, from, to);
        } else {
            throw new IllegalArgumentException("At least one of the parameters (traderId, ticker) should be specified.");
        }
        return TradeDataService.asHtmlTable(result);
    }

    @GetMapping(value = "/data/select/html", produces = MediaType.TEXT_HTML_VALUE)
    public String selectHtmlTable(@RequestParam("query") String query) {
        List<Trade> trades = tradeDataService.getTrades(query);
        return TradeDataService.asHtmlTable(trades);
    }

    @GetMapping(value = "/data/select/json", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Trade> selectJson(@RequestParam("query") String query) {
        return tradeDataService.getTrades(query);
    }


}

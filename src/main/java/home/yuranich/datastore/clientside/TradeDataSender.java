package home.yuranich.datastore.clientside;

import com.fasterxml.jackson.databind.ObjectMapper;
import home.yuranich.datastore.model.Trade;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Random;

public class TradeDataSender {
    public static void main(String[] args) {
        String url = "http://yuranich-server.ddns.net:8080/data/put";

        long startTime = System.currentTimeMillis();
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPut put = new HttpPut(url);
            Random random = new Random(42L);
            LocalDateTime time = LocalDateTime.now();
            ObjectMapper objectMapper = new ObjectMapper();
            int start = 1;
            int end = 101;
            for (int i = start; i < end; i++) {
                long ts = time.plusSeconds(i).toInstant(ZoneOffset.UTC).toEpochMilli();
                StringEntity entity = new StringEntity(objectMapper.writeValueAsString(
                        new Trade(random.nextInt(20), ts, random.nextFloat(), random.nextInt(2000), "apple")
                ));
                entity.setContentType("application/json");
                put.setEntity(entity);
                client.execute(put);
                put.releaseConnection();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Puts running time in seconds: " + (endTime - startTime) / 1000.0);

    }
}

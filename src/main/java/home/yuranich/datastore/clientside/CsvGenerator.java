package home.yuranich.datastore.clientside;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CsvGenerator {
    private static final String FILENAME = "batch_example.csv";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy, hh:mm:ss a");

    public static void main(String[] args) {
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(FILENAME))) {
            LocalDateTime now = LocalDateTime.now();
            StringBuilder builder = new StringBuilder(3000);
            builder.append("Trader Id,Date,Share price (P),Overall position\n");
            for (int i = 1; i < 100; i++) {
                builder.append(i)
                        .append(",\"")
                        .append(now.plusHours(i).format(formatter))
                        .append("\",")
                        .append(i*10/4.0)
                        .append(',')
                        .append(i * 100)
                        .append('\n');
            }
            writer.write(builder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
